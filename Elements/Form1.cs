﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elements
{
    public partial class Form1 : Form
    {
        private string _elementName;
        private string _ProposedSymbolName;
        private string _errorMessage = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
                if(Valid())
                {
                    txtSymbol.Text = txtProposedSymbol.Text;
                    MessageBox.Show("New Symbol generated");
                }
                else MessageBox.Show(_errorMessage);

        }

        public bool Valid()
        {
            _errorMessage = "";

            #region Check Empty Strings in textboxes
            if (string.IsNullOrEmpty(txtElement.Text) || string.IsNullOrEmpty(txtProposedSymbol.Text)) 
            {
                _errorMessage = "Please enter Element and Symbol Name before procceding";
                return false;
            }

            #endregion

            #region Check if first element and symbol characters are Uppercases

            if (!char.IsUpper(txtElement.Text.FirstOrDefault()) || !char.IsUpper(txtProposedSymbol.Text.FirstOrDefault())) 
            {
                _errorMessage = "Please make sure the first letter is upper case";
                return false;
            }
            #endregion

            #region Check if the rest of the string in lower case 
            foreach (var item in txtElement.Text.Skip(1))
            {
                if (char.IsUpper(item))
                {
                    _errorMessage = "Please ensure only the first letter is upper case";
                    return false;
                }
            }
            foreach (var item in txtProposedSymbol.Text.Skip(1))
            {
                if (char.IsUpper(item))
                {
                    _errorMessage = "Please ensure only the first letter is upper case";
                    return false;
                }
            }

            #endregion

            #region Rule 1 : Check if symbol contains two letters
            if (txtProposedSymbol.Text.Length != 2)
            {
                _errorMessage = "Symbol must be two letters in total";
                return false;
            }

            #endregion

            //At this point we know both element and symbol name exist
            _elementName = txtElement.Text.ToLower();
            _ProposedSymbolName = txtProposedSymbol.Text.ToLower();
            string FirstSymbolLetter = _ProposedSymbolName[0].ToString();
            string SecondSymbolLetter = _ProposedSymbolName[1].ToString();
            
            #region Rule 2 : check if both symbol letters exist in the element name

            if (!_elementName.ToLower().Contains(FirstSymbolLetter) || !_elementName.ToLower().Contains(SecondSymbolLetter))
            {
                _errorMessage = "The letters in the symbol must both exist in the element name";
                return false;
            }

            #endregion


            int FirstSymbolPosition, SecondSymbolPosition;

            #region Rule 3: Check the order in which symbols appear 

            FirstSymbolPosition = _elementName.LastIndexOf(FirstSymbolLetter); 
            SecondSymbolPosition = _elementName.LastIndexOf(SecondSymbolLetter);

            if (FirstSymbolPosition > SecondSymbolPosition)
            {
                _errorMessage = "The two letters must appear in order as they are in the element name";
                return false;
            }

            #endregion

            #region Rule 4 : Check if same symbol names appear twice in element Name
            //Rule 4
            if (FirstSymbolLetter == SecondSymbolLetter)
            {
                int CountNumberInElement = 0;
                foreach (var item in _elementName)
                {
                    if (item == char.Parse(FirstSymbolLetter))
                    {
                        CountNumberInElement++;
                    }
                }
                if (CountNumberInElement < 2)
                {
                    _errorMessage = "They are two letters in the symbol that are the same, It must appear twice in the element name";
                    return false;
                }
            }
            #endregion

            //If checks pass symbol is valid for element
            return true;
        }
    }
}
