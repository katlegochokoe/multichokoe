﻿namespace Elements
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGetSymbol = new System.Windows.Forms.Button();
            this.txtElement = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSymbol = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProposedSymbol = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnGetSymbol
            // 
            this.btnGetSymbol.Location = new System.Drawing.Point(145, 198);
            this.btnGetSymbol.Name = "btnGetSymbol";
            this.btnGetSymbol.Size = new System.Drawing.Size(152, 30);
            this.btnGetSymbol.TabIndex = 0;
            this.btnGetSymbol.Text = "Click";
            this.btnGetSymbol.UseVisualStyleBackColor = true;
            this.btnGetSymbol.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtElement
            // 
            this.txtElement.Location = new System.Drawing.Point(90, 44);
            this.txtElement.Name = "txtElement";
            this.txtElement.Size = new System.Drawing.Size(119, 20);
            this.txtElement.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Enter element";
            // 
            // txtSymbol
            // 
            this.txtSymbol.Location = new System.Drawing.Point(12, 126);
            this.txtSymbol.Multiline = true;
            this.txtSymbol.Name = "txtSymbol";
            this.txtSymbol.Size = new System.Drawing.Size(84, 21);
            this.txtSymbol.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Symbol";
            // 
            // txtProposedSymbol
            // 
            this.txtProposedSymbol.Location = new System.Drawing.Point(316, 44);
            this.txtProposedSymbol.Name = "txtProposedSymbol";
            this.txtProposedSymbol.Size = new System.Drawing.Size(43, 20);
            this.txtProposedSymbol.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(215, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Proposed Sybmbol";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 249);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtProposedSymbol);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSymbol);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtElement);
            this.Controls.Add(this.btnGetSymbol);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGetSymbol;
        private System.Windows.Forms.TextBox txtElement;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSymbol;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProposedSymbol;
        private System.Windows.Forms.Label label3;
    }
}

